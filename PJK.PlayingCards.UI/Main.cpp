
// Playing Cards
// Phoenix Kaufmann

// Header files
#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

// Card rankings
enum Rank
{
	Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, King, Queen, Ace
};

// Card suits
enum Suit
{
	Clubs, Diamonds, Hearts, Spades
};

struct Card
{
	Rank Rank;
	Suit Suit;
};

int main()
{


	(void)_getch();
	return 0;
}